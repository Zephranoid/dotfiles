#!/bin/bash
dir=$(pwd)
ln -sf $dir/i3/config ~/.config/i3/config
ln -sf $dir/polybar/config ~/.config/polybar/config
ln -sf $dir/compton/compton.conf ~/.config/compton/compton.conf
ln -sf $dir/dunst/dunstrc ~/.config/dunst/dunstrc
ln -sf $dir/qutebrowser/config.py ~/.config/qutebrowser/config.py
ln -sf $dir/rofi/config ~/.config/rofi/config
ln -sf $dir/termite/config ~/.config/termite/config
ln -sf $dir/vim/theme.vim ~/.vim/colors/theme.vim
ln -sf $dir/vim/vimrc ~/.vimrc
ln -sf $dir/rofi/custom.rasi ~/.config/rofi/custom.rasi
ln -sf $dir/zsh/zshrc ~/.zshrc
ln -sf $dir/conky/conky.conf ~/.config/conky/conky.conf
ln -sf $dir/qutebrowser/qutewal.py ~/.config/qutebrowser/qutewal.py
